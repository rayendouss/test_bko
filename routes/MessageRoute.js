const express = require('express');
const router = express.Router();
const MessageService = require('../services/MessageService');
const MessageMiddleware=require('../middleware/MessageMiddleware')


/**
 *
 * @route GET message/
 * @desc Affiche les données de la table message
 * @access Public
 * @returns list de messages sous forme JSON
 *
 */
 router.get('/', MessageService.GetAllMessages);

 /**
 *
 * @route GET /message/{id}
 * @desc Affiche les données de la table message parc{id}.
 * @access Public
 * @returns message by id  sous forme JSON
 *
 */
 router.get('/:id', MessageService.GetMessageById);

 /**
 *
 * @route Post /message/addMessage
 * @desc Ajoute une nouvelle ligne dans la table message.
 * @access Public
 * @returns la resultat de requete success ou echec apres une validation
 *
 */
router.post('/addMessage', MessageMiddleware, MessageService.AddMessage);



 module.exports = router;