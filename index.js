const express = require('express');
const cors = require('cors');
const app = express();
const port = 5000;
const dbConnect = require('./config/dbconfig')
const messageRoute = require('./routes/MessageRoute')
require('dotenv').config();

app.use(cors());
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use('/message', messageRoute);

app.listen(port, () => {
    console.log(
      `Server App listening at http://localhost:${process.env.SERVER_PORT}`
    );
  })