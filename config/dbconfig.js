const mysql = require('mysql');

const config = {
    db: {
      host: 'localhost',
      user: 'root',
      password: '',
      port: '3306',
      database: 'testbko',
    },
  };

const dbConnect = mysql.createConnection(config.db);
dbConnect.connect(function (err) {
  if (err) throw err;
  console.log('Database Connected!');
});

module.exports = dbConnect;