-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2022 at 12:21 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testbko`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `nom`, `email`, `tel`, `message`) VALUES
(1, 'bul ko', 'testBko1@bulko.net', '0770707070', 'Pony ipsum dolor sit amet it needs to be about 20% cooler. Parasprite Cheerilee friend Ms. Peachbottom, Cheese Sandwich sun Zecora dragon Pumpkin Cake Trixie Spitfire Princess Luna. Tail Big McIntosh Flam Scootaloo, Donut Joe Philomena rainbow power Gummy Cranky Doodle Donkey Wonderbolts breezies Zecora friend. Lightning Dust Prim Hemline Wonderbolts Nightmare Moon Ms. Harshwhinny laugher kindness.'),
(2, 'bul ko2', 'testBko2@bulko.net', '0770707070', 'Pony ipsum dolor sit amet magic nulla adipisicing apples commodo magna. Pony Daring Do Matilda Cheese Sandwich chaos dolor, aliquip hoof Spitfire veniam Dr. Caballeron mane Maud Pie. Featherweight Bon Bon Opalescence Matilda hay. Alicorn Everfree Forest magic culpa sunt Twist occaecat proident pony consequat. Cupcake non Flash Sentry, enim dolore ex aliqua muffin Zecora wing pony chaos Sonata Dusk horn irure.'),
(3, '', 'testUnknow@bulko.net', '0770707072', ''),
(4, 'test', 'testemail', '12345', 'aa'),
(5, 'test', 'testemail', '12345', 'aa'),
(6, 'test', 'testemail', '12345', 'aa'),
(7, 'test', 'testemail', '12345', 'aa'),
(8, 'test', 'testemail', '12345', 'aa'),
(9, 'test', 'testemail', '12345', 'aa'),
(10, 'aa', 'mohamedrayane.douss@', '24989322', 'aaaa'),
(11, 'aa', 'mohamedrayane.douss@', '24989322', 'aaa'),
(12, 'rayen', 'mohamedrayane.douss@', '24989322', 'aaaaaa'),
(13, 'test', 'testemail', '12345', 'aa'),
(14, 'test', 'testemail', '12345', 'aa'),
(15, 'test', 'testemail', '12345', 'aa'),
(16, 'test', 'testemail', '12345', 'aa'),
(17, 'test', 'testemail', '12345', 'aa'),
(18, 'test', 'testemail', '12345', 'aa'),
(19, 'test', 'testemail', '12345', 'aa'),
(20, 'test', 'testemail', '12345', 'aa'),
(21, 'test', 'testemail', '12345', 'aa'),
(22, 'test', 'testemail', '12345', 'aa'),
(23, 'test', 'testemail', '0123456789', 'aa'),
(24, 'test', 'testemail@mail.com', '0123456780', 'aa'),
(25, 'test', 'testemail@mail.com', '0234567801', 'aa'),
(26, 'rayen', 'rayen@gmail.com', '0123456789', 'aaaa'),
(27, 'rayen', 'mohamedrayane.douss@', '0123456789', 'aaaa'),
(28, 'aaaaa', 'mohamedrayane.douss@', '0123456789', 'aaaaaaa'),
(29, 'aa', 'mohamedrayane.douss@', '0123456789', 'Message'),
(30, 'rayen', 'mohamedrayane.douss@', '0123456789', 'message'),
(31, '', 'mohamedrayane.douss@', '0123456789', ''),
(32, 'rayen', 'mohamedrayane.douss@', '0123456789', 'aaaaa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
