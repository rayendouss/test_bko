const nodemailer = require('nodemailer')
const SendMail = async (data) => {
    
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: process.env.EMAIL,
               pass: process.env.MDP
           }
       });

       let info = await transporter.sendMail({
        from: process.env.EMAIL, 
        to: 'info@bulko.net', 
        subject: 'From '+ data.email, 
        text: 
        `Nom :${data.nom}
         Email envoyé par :${data.email} ,
         Numéro de téléphone : ${data.tel} ,
         Message : ${data.message}
        `,
      });

}

module.exports = SendMail;
