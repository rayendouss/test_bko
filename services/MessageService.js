const Message = require('../models/Message');
const sendMail= require('../services/MailService')

var dbConnect = require('../config/dbconfig');

const GetAllMessages = async (req, res) => {
    dbConnect.query('Select * from message', function (err, messages) {
        if (err) {
            return res.json({ error: err });
        } else {
         
           return res.json({ result: messages });
        }
      });
  };

  const GetMessageById = async (req, res) => {


      dbConnect.query('Select * from message where id = ? ', req.params.id, function (err, messages) {
        if (err) {
            return res.json({ error: err });
        } else {
           return res.json({ result: messages });
        }
      });
  };  

  const AddMessage = async (req, res)=>{
   
    dbConnect.query('INSERT INTO message set ?', req.body, function (err, messages) {
        if (err) {
            return res.json({ error: err });
        } else {
            sendMail(req.body)
           return res.json({result:"message added"});
        }
      });
  }


  module.exports = {
    GetAllMessages,
     GetMessageById,
     AddMessage,
  };
