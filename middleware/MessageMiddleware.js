
const Joi = require('joi');

const schema = Joi.object({
    nom: Joi.string()
    .allow(null, '')
    ,
        

    email: Joi.string()
    .email().message('E-mail incorrecte')
    .required()
    ,
    tel: Joi
   
    .string()
   .regex(/^0[0-9]*$/).message('Téléphone à 10 chiffres commençant par 0 ')
    .max(10).message('FTéléphone à 10 chiffres ')
    .min(10).message('Téléphone à 10 chiffres ')
    .required()
    
    ,
    message: Joi.string()
    .allow(null, '')
    ,
    })
    
        
async function MessageMiddleware(req, res, next) {

  
  
    try {
        const value = await schema.validateAsync(req.body);
      next();
    } catch (err) {
      res.status(422).json({ errors: err.details[0].message});
    }
  
  }
  module.exports =  MessageMiddleware ;